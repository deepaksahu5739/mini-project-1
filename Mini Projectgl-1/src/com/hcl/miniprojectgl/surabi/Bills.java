package com.hcl.miniprojectgl.surabi;

import java.sql.Date;
import java.util.List;

public class Bills {

	private String name;
	private List<Menus> items;
	private double cost;
	private Date time;
	
	public Bills() { 
		
	}
    
	public Bills(String name, List<Menus> items, double cost, Date time){
		super();
		
		this.name = name;
		this.items = items;
		this.cost = cost;
		this.time = time;
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Menus> getMenu() {
		return items;
	}
	public void setMenu(List<Menus> selectedMenu) {
		this.items = selectedMenu;
	}
	
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getTime() {
		return time;
	}

	public void setTime(java.util.Date date) {
	
	}
		

}
package com.hcl.miniprojectgl.surabi;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class Main {
	
	
	public static void main(String[] args) {
		
		 Scanner sc = new Scanner(System.in);
		 Date time = new Date();
		 List<Bills> bills = new ArrayList<Bills>();
		 boolean finalOrder;
		 List<Menus> items = new ArrayList<Menus>();
		 
		 Menus menu1 = new Menus(1, "Rajma Chawal", 2, 150.0);
		 Menus menu2 = new Menus(2, "Momos", 2, 190.0);
		 Menus menu3 = new Menus(3, "Red Thai Curry", 2, 180.0);
		 Menus menu4 = new Menus(4, "Chaap", 2, 190.0);
		 Menus menu5 = new Menus(5, "Chilli Potato", 1, 250.0);
		 
		 items.add(menu1);
		 items.add(menu2);
		 items.add(menu3);
		 items.add(menu4);
		 items.add(menu5);
		 
		 System.out.println("Welcome to Surabi Restaurants \n"
		 		+ "***************************************");
		 boolean flag=true;
		 
		 while(flag) {
			 System.out.println("Please Enter Your Credentials");
			 
			 System.out.println("Email id = ");
			 String email = sc.next();
			 
			 System.out.println("Password = ");
			 String password = sc.next();
			 
			 String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);
		
			 System.out.println("Please Enter A if you are Admin and U if you are User");
			 String au = sc.next();
			 
			 Bills bill = new Bills();
			 List<Menus> selectedMenu = new ArrayList<Menus>();
			 
			 double totalCost = 0;
			 
			 Date date = new Date();
			 
			 ZonedDateTime time1 = ZonedDateTime.now();
			 DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH : mm:ss zzz yyyy");
			 String currentTime = time1.format(f);
			 
			 if(au.equals("U") || au.equals("U")) {
				 System.out.println("Welcome " + name);
				
				do {
					  System.out.println("Today's Menu :");
					  items.stream().forEach(i -> System.out.println(i));
					  System.out.println("Enter the Menu Item Number :");
					  int number = sc.nextInt();
					  
					  if(number ==1) {
						  selectedMenu.add(menu1);
						  totalCost += menu1.getPrice();
					  }
					  else if (number ==2) {
						  selectedMenu.add(menu2);
						  totalCost += menu2.getPrice(); 
					  } else if (number == 3) {
						  selectedMenu.add(menu3);
						  totalCost += menu3.getPrice();
					  }else if (number == 4 ) {
						  selectedMenu.add(menu4);
						  totalCost += menu4.getPrice();
					  }else {
						  selectedMenu.add(menu5);
						  totalCost += menu5.getPrice();
					  }
					  
					  System.out.println("Press 0 to show bill\n Press 1 to order more");
					  
					  int opt = sc.nextInt();
						if (opt == 0)
							finalOrder = false;
						else
							finalOrder = true;

					} while (finalOrder);
					

					System.out.println("Thanks Mr " + name + " for dining in with Surabi ");
					System.out.println("Items you have Selected");
					selectedMenu.stream().forEach(e -> System.out.println(e));
					System.out.println("Your Total bill will be " + totalCost);

				
					bill.setName(name);
					bill.setCost(totalCost);
					bill.setMenu(selectedMenu);
				    bill.setTime(date);
					bills.add(bill);

				} else if (au.equals("A") || au.equals("a")) {
					System.out.println("Welcome Admin");
					System.out.println(
							"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
					int value = sc.nextInt();
					switch (value) {
					case 1:
						if ( !bills.isEmpty()) {
							for (Bills b : bills) {
								if(b.getTime().getDate()==time.getDate()) {
								//if(b.getTime().getDate()==time.getDate()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getMenu());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
							}
						} else
							System.out.println("No Bills today");
						break;

					case 2:
						if (!bills.isEmpty()) {
							for (Bills bill0 : bills) {
								if (bill.getTime().getMonth()==time.getMonth()) {
								System.out.println("\nUsername :- " + bill0.getName());
								System.out.println("Items :- " + bill0.getMenu());
								System.out.println("Total :- " + bill0.getCost());
								System.out.println("Date " + bill0.getTime() + "\n");
							}
							}
						} else
							System.out.println("No Bills for this month");
						break;

					case 3:
						if (!bills.isEmpty()) {
							for (Bills bill1 : bills) {
								System.out.println("\nUsername :- " + bill1.getName());
								System.out.println("Items :- " + bill1.getMenu());
								System.out.println("Total :- " + bill1.getCost());
								System.out.println("Date " + bill1.getTime() + "\n");
							}
						} 
						
						else
							System.out.println("there is no bills");

						break;

					default:
						System.out.println("Invalid Value");
						System.exit(1);
					}
				} 
				else if (au.equals("L") || au.equals("l")) {
					System.exit(1);
					
				}
				else 
				{
					System.out.println("Invalid");
				}

			}

		}

}
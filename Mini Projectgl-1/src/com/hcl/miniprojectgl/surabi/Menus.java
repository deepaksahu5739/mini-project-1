package com.hcl.miniprojectgl.surabi;

public class Menus {
	
	private int ItemID;
	private String Name;
	private int Quantity;
	private double Price;
	
	public Menus(int ItemID, String Name, int Quantity, double Price)
	{
		super();
		
		this.ItemID=ItemID;
		this.Name= Name;
		this.Price= Price;
		this.Quantity= Quantity;
	}

	public int getItemID() {
		return ItemID;
	}

	public void setItemID(int itemID) {
		this.ItemID = itemID;
	}

	public String getName() {
		return Name;
	}

	public void setItemName(String Name) {
		this.Name = Name;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setItemQuantity(int Quantity) {
		this.Quantity = Quantity;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double Price) {
		this.Price = Price;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "" + ItemID + " " + Name + " " + Quantity + " " + Price;
	}

	

}